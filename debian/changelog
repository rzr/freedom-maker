freedom-maker (0.33) unstable; urgency=medium

  [ James Valleroy ]
  * vagrant: Don't modify /etc/security/access.conf
  * vagrant: Store sha256 hash
  * Drop non-free component
  * Cleanup reference to test target
  * debian: Generate manpages during build
  * Update manual pages
  * Show end of log if debootstrap fails
  * arm: Increase boot size to 256 MiB
  * armhf: Replace u-boot package with specific variant
  * builder: Split 'free' into 2 properties
  * vagrant: Include contrib for virtualbox-guest-utils
  * amd64, i386: Add {amd64,intel}-microcode packages
  * tests: Fix virtualbox test
  * vagrant: Fix script paths
  * Update copyright year
  * Follows Debian policy v4.6.2

  [ Sunil Mohan Adapa ]
  * raspberry2, raspberry3: Increase firmware partition size to 256MiB
  * library: Increase the compression RAM usage limit on latest xz
  * releases: Create a new module to handle release specific information
  * releases: Switch to bookworm as the latest stable
  * builder, vm: Increase size of the image for VMs
  * vm: Fix failure while returning image size for vagrant images
  * *: Remove use of cliapp library as it seems unmaintained
  * *: Fix mypy errors
  * Use pyproject.toml instead of setup.py
  * *: Convert tests to use pytest

 -- James Valleroy <jvalleroy@mailbox.org>  Fri, 09 Feb 2024 12:44:20 -0500

freedom-maker (0.32) unstable; urgency=medium

  [ James Valleroy ]
  * raspberry64: Add target for Raspberry Pi 64-bit
  * Add non-free-firmware component to non-free images
  * d/copyright: Update source URL and copyright years
  * Add systemd as alt dependency to binfmt-support (Closes: #1023395)
  * Set standards version to 4.6.1
  * Drop alt dependency on old util-linux

  [ Sunil Mohan Adapa ]
  * raspberry64: Add Wi-Fi firmware into the image
  * README: Update list of dependencies from debian/control file
  * builder: Drop free status from image names

  [ Benedek Nagy ]
  * README.md: Add raspberry64 to the target list

 -- James Valleroy <jvalleroy@mailbox.org>  Tue, 13 Dec 2022 11:22:05 -0500

freedom-maker (0.31) unstable; urgency=medium

  [ James Valleroy ]
  * debian: Add gbp tag config
  * vagrant: Use poweroff in case shutdown does not work

  [ Sunil Mohan Adapa ]
  * internal, library: Use debootstrap instead of qemu-debootstrap
  * library: Enable transparent compression of btrfs file systems

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 08 Nov 2021 19:11:31 -0500

freedom-maker (0.30) unstable; urgency=medium

  [ Diederik de Haas ]
  * Synchronize and sort supported targets.
  * Change issue tracker to point to salsa.
  * Fix incorrect example 2.
  * rasbperry_pi{2,3}: Fix Raspberry Pi firmware package name.

  [ James Valleroy ]
  * Update manpage from xml
  * tests: Run --list-modules command in VM
  * tests: Ignore errors while cleaning up VM
  * vagrant: Fix incorrect parameter to subprocess.run
  * tests: Send shutdown command to VM
  * Add option to specify additional release components
  * vagrant: Drop virtualbox-guest-dkms package
  * vagrant: Don't modify apt sources list
  * builder: Check added components to set free_tag
  * debian: Set standards version to 4.6.0

  [ Joseph Nuthalapati ]
  * Change stable from buster to bullseye
  * Parametrize codename of the current stable release

  [ Debian Janitor ]
  * d/control: Remove constraints unnecessary since buster

  [ Sunil Mohan Adapa ]
  * builder: Allow build stamp to be empty
  * builders: Align start and end of all partitions to 4MiB
  * vagrant: Don't install linux-headers
  * builder: Minor refactor to compute release components

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 30 Aug 2021 19:25:43 -0400

freedom-maker (0.29) experimental; urgency=medium

  [ Sunil Mohan Adapa ]
  * builder: internal: Allow creating an EFI partition
  * builder: internal: Allow building images with GPT partition table
  * arm64: Add a builder for ARM64 universal images based on UEFI
  * library: Allow specifying GRUB target platform
  * arm64: Factor out code related to ARM EFI so it can be reused
  * armhf: New target to build generic UEFI images for all armhf boards
  * *: Switch to SPDX license identifier
  * arm_efi: Don't allow grub to update NVRAM variables
  * README, man: Add information about new targets - arm64, armhf
  * man: Regenerate manual page

  [ James Valleroy ]
  * debian: Add gbp dch config
  * debian: Set Standards-Version to 4.5.1

  [ Joseph Nuthalapati ]
  * stable: Remove udiskie using `apt autoremove`
  * Allow build dependencies to be included in images
  * YAPF formatting
  * with-build-dep: Remove implicit image size setting
  * with-build-dep: Clean up cloned freedombox repo
  * with-build-dep: Remove check for stable
  * library: Install xz-utils, a dep of gdebi-core
  * library: Use apt instead of gdbi for local debs
  * internal: Cleanup custom and backported installs

  [ Debian Janitor ]
  * debian/tests: Replace use of deprecated $ADTTMP with $AUTOPKGTEST_TMP

  [ Diederik de Haas ]
  * Fix various spelling errors in comments
  * Start first partition at 4MiB for proper alignment.

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 19 Apr 2021 19:48:25 -0400

freedom-maker (0.28) unstable; urgency=medium

  [ James Valleroy ]
  * vagrant: Skip missing packages on stable and testing
  * vagrant: Avoid possible recursion in run_vm_command
  * vagrant: Try to clean up old VM

  [ Joseph Nuthalapati ]
  * Add qemu-utils as a dependency

 -- James Valleroy <jvalleroy@mailbox.org>  Tue, 30 Jun 2020 09:58:31 -0400

freedom-maker (0.27) unstable; urgency=medium

  [ James Valleroy ]
  * builders: Add orange-pi-zero target
  * debian: Add deboostrap as dependency
  * Run yapf
  * Fix some flake8 issues
  * vagrant: Fail build when VM command has error
  * builder: Remove firmware-ath9k-htc from BASE_PACKAGES
  * d/tests/control: Add unit tests
  * application: Allow backports for buster, as well as stable
  * backports: Add --disable-backports option to replace --enable-backports
  * d/control: Switch to debhelper compat version 13

  [ Joseph Nuthalapati ]
  * Enable backports and install latest freedombox.deb
  * application: Check for incompatible arguments
  * Install documentation packages from backports

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 04 May 2020 08:41:20 -0400

freedom-maker (0.26) unstable; urgency=medium

  [ James Valleroy ]
  * d/rules: Don't skip tests during build
  * ci: Switch to salsa-ci pipeline
  * ci: Move to debian/salsa-ci.yml
  * raspberry: Use raspi-firmware package in testing and unstable
  * d/control: Update standards version to 4.5.0

  [ Veiko Aasa ]
  * internal: Make directory /var/lib/freedombox readable by other users

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 10 Feb 2020 18:28:06 -0500

freedom-maker (0.25) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * Remove references to freedombox-setup
  * library: Aggressively re-balance btrfs to avoid running out of space
  * internal: library: Kill processes before attempting unmount
  * application: Remove regressed source code options
  * application: cosmetic: yapf formatting

  [ James Valleroy ]
  * debian: Switch to debhelper-compat
  * d/control: Update Standards-Version to 4.4.1
  * d/control: Add Rules-Requires-Root

 -- James Valleroy <jvalleroy@mailbox.org>  Fri, 04 Oct 2019 07:59:14 -0400

freedom-maker (0.24) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * internal: Fix issue with using builder specific packages
  * pine64+: Add new builder for Pine64+ board
  * Update tests for the split udev command
  * pine64-lts: Add new builder for Pine64 LTS board
  * Replace atf-allwinner with arm-trusted-firmware
  * Yapf and isort changes
  * internal: Fix issue with premature disk fulls by btrfs
  * all: Merge next release packages into basic packages

  [ Joseph Nuthalapati ]
  * ci: Remove dependency "pxz" from GitLab CI script
  * Add Pine64+ to list of targets in README.md

  [ Florian Boor ]
  * Split udev command in order to improve compatibility
  * Add support for building Lamobo R1 SD card images

  [ James Valleroy ]
  * vagrant: Add ncurses-term and byobu
  * vagrant: Add packages needed for testing
  * README: Add some missing dependencies
  * library: Handle new name for security repository
  * d/control: Bump standards version to 4.4.0

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 15 Jul 2019 19:49:17 -0400

freedom-maker (0.23) unstable; urgency=medium

  [ Juan Carlos Romero ]
  * vagrant-package: Fix typo in error message

  [ Sunil Mohan Adapa ]
  * passwd-in-image: Fix typo in error message

  [ Joseph Nuthalapati ]
  * library: Remove dependency - pxz (Closes: #919809)

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 28 Jan 2019 16:13:29 -0500

freedom-maker (0.22) unstable; urgency=medium

  [ James Valleroy ]
  * internal: Fix initialization of custom_freedombox
  * debian: Update debhelper compat version to 12

  [ Sunil Mohan Adapa ]
  * Remove dreamplug and raspberry pi 1 targets
  * internal: Remove explicit dependency on initramfs-tools

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 14 Jan 2019 17:11:26 -0500

freedom-maker (0.21) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * raspberry2/3: Fix boot issue caused due to kernel7.img
  * internal: Allow custom freedombox package to be provided

  [ James Valleroy ]
  * debian: Standards-Version is now 4.3.0

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 31 Dec 2018 18:45:17 -0500

freedom-maker (0.20) unstable; urgency=medium

  [ James Valleroy ]
  * Remove Travis CI, replaced by Gitlab CI
  * ci: Prevent installing fuse
  * Add raspberry3-b-plus target
  * Finish cleanup of vmdebootstrap
  * Bump Standards-Version to 4.2.1

  [ Matthias Weiler ]
  * Fix outdated/incorrect README.md

  [ Sébastien Barthélémy ]
  * Fix typo in CLI --help

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 03 Dec 2018 21:12:29 -0500

freedom-maker (0.19) unstable; urgency=medium

  * Fix test dependency
  * Bump standards version

 -- James Valleroy <jvalleroy@mailbox.org>  Tue, 17 Jul 2018 20:52:11 -0400

freedom-maker (0.18) unstable; urgency=medium

  [ Joseph Nuthalapati ]
  * Add option to skip image compression

 -- James Valleroy <jvalleroy@mailbox.org>  Tue, 19 Jun 2018 19:48:10 -0400

freedom-maker (0.17) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * internal: Set the boot flag only on boot partition
  * Make internal backend default for Raspberry Pi 2/3
  * internal: Properly set kernel options with flash-kernel
  * Switch beaglebone to internal backend by default
  * internal: Handle no kernel and no boot loader
  * internal: Finish Raspberry Pi implementation
  * internal: Fail build on firmware install fail for rpi2/3
  * internal: Added initramfs-tools as explicit dependency
  * Switch to internal backend for Raspberry Pi
  * internal: Finish implementation for Dreamplug
  * internal: Don't call flash-kernel on /boot with vfat filesystem
  * Switch dreamplug to internal backend by default
  * Make internal backend the default backend
  * Unmount /etc/machine-id after vmdebootstrap
  * Fix issue leading to unbootable grub configuration
  * Minor fix to run_in_chroot to return stdout value
  * pcduino3: Fix target name in documentation
  * Remove vmdebootstrap backend
  * Log only to console, simplify logging
  * Remove skip step checking
  * Implement compress and sign library methods
  * Move temporary image creation to library
  * Factor out boot loader installation for A20 boards
  * Split builders into separate modules
  * Simplify dependency documentation
  * Remove a use of sudo
  * Add missing logging during library actions

  [ James Valleroy ]
  * builders: Minor fixes for comments and spelling
  * Add dependency on fdisk (Closes: #872130)

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 04 Jun 2018 16:52:43 -0400

freedom-maker (0.16) unstable; urgency=medium

  [ James Valleroy ]
  * Fix tests for kernel flavor and filesystems
  * tests: Skip virtualbox targets if VirtualBox is not available
  * Add gitlab CI
  * Add pipeline and package badges

  [ Sunil Mohan Adapa ]
  * Ignore errors when force releasing loop device
  * Limit size of tmpfs mount to prevent issue during zerofill
  * Don't force when removing device using dmsetup
  * Fix image getting truncated during bootloader install
  * yapf, isort changes
  * Use logger.warning instead of logger.warn
  * internal: Run flash-kernel explicitly
  * internal: Fix flash-kernel machine name path
  * internal: Fix making fstab entry for boot partition
  * Switch to internal backend for all a20 boards
  * internal: Run update-initramfs towards the end
  * internal: Run rpi boot script properly

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 21 May 2018 18:26:16 -0400

freedom-maker (0.15) unstable; urgency=medium

  [ Joseph Nuthalapati ]
  * Fix naming convention for virtualbox log file

  [ James Valleroy ]
  * wifi: Use packaged ath9k firmware in testing/unstable
  * debian: Add python3-cliapp as build-depend
  * autopkgtest: Add depend on python3-pkg-resources
  * debian: Bump standards version to 4.1.4

  [ Sunil Mohan Adapa ]
  * Indentation and other minor fixes
  * Add kernel flavor explicitly for all targets
  * Add argument to choose backends
  * Change boot size for ARM images to 128MiB from 128MB
  * Implement an internal backend instead of vmdb2
  * Default to internal backend for amd64/i386 images
  * debian: Add additional dependencies of internal backend
  * debian: Update VCS URLs
  * debian: Update maintainer field
  * Update debhelper compatibility level to 11
  * Use secure URL for copyright format URI
  * debian: Add autopkgtest tests
  * Various fixes for internal builder

 -- James Valleroy <jvalleroy@mailbox.org>  Sat, 28 Apr 2018 13:43:34 -0400

freedom-maker (0.14) unstable; urgency=medium

  [ Sébastien Barthélémy ]
  * readme: Fix following switch from tar to img
  * readme: Decompress the image on the fly

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 26 Mar 2018 19:28:52 -0400

freedom-maker (0.13) unstable; urgency=medium

  * vagrant: Add test dependencies

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 12 Mar 2018 17:09:57 -0400

freedom-maker (0.12) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * Remove systemd-networkd DHCP configuration from vmdebootstrap

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 26 Feb 2018 17:23:15 +0100

freedom-maker (0.11) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * raspberry2/3: Use stable cloned MAC address on stable release

 -- James Valleroy <jvalleroy@mailbox.org>  Tue, 13 Feb 2018 06:27:30 -0500

freedom-maker (0.10) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * Add option to build the image in RAM.
  * Remove eatmydata speedup.
  * Cleanup loop devices that vmdebootstrap does not.
  * Perform more operations with sudo.

 -- James Valleroy <jvalleroy@mailbox.org>  Mon, 15 Jan 2018 10:50:54 -0500

freedom-maker (0.9) unstable; urgency=medium

  [ Sunil Mohan Adapa ]
  * Remove dependency on extlinux. (Closes: #869203)
  * Add myself to list of uploaders.
  * Depend on btrfs-progs instead of btrfs-tools. (Closes: #869223)

  [ James Valleroy ]
  * Add myself to list of uploaders.
  * Bump standards version.
  * Replace priority extra with priority optional.

 -- James Valleroy <jvalleroy@mailbox.org>  Tue, 02 Jan 2018 19:30:22 -0500

freedom-maker (0.8) unstable; urgency=low

  [ Joseph Nuthalapati ]
  * Initial release. (Closes: #864764)

 -- Federico Ceratto <federico@debian.org>  Sun, 18 Jun 2017 16:00:03 +0100
