# SPDX-License-Identifier: GPL-3.0-or-later
"""
Information about various Debian releases.
"""

STABLE_CODENAME = 'bookworm'

STABLE_DISTRIBUTIONS = ['stable', 'bullseye', 'bookworm']


def is_stable(distribution: str):
    """Return whether the selected distribution is stable."""
    return distribution in STABLE_DISTRIBUTIONS
